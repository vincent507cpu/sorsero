# NLP Ontologist technical test

# Task: Find hyponyms and hypernyms

# Description: 
Using WordNet, extract a list of hyponyms and hypernyms for recognisable words in the following text:

"\tThe differential diagnosis of multiple myeloma usually involves the
spectrum of plasma cell proliferative disorders shown in Table
3.1,2,6,13,14,20 A full evaluation will help classify where a patient
falls in this spectrum.\n The differential diagnosis of bone lesions
includes primary or metastatic cancer, benign bone lesions,
osteoporotic compression fracture, and other bone conditions.21,22 The
full differential diagnosis for patients presenting with fatigue,
unexplained weight loss, or hypercalcemia is broad and beyond the
scope of this article.23-25"

# Prerequisites: 
First install NLTK and download required packages. In shell environment: 
```bash
pip install NLTK==3.4.5
```
then in Python:
```python
import nltk
nltk.download() # download all in once, or download them on by one:
# nltk.download('stopwords')
# nltk.download('punkt')
# nltk.download('averaged_perceptron_tagger')
# nltk.download('wordnet')
```

# My idea:
I try to make this script as robust as possible. I would like to lemmatize these words to better get syponyms and hypernyms for noun, verb, adj and adv word; but it certainly depends on different requirements. 
- Preprocess the text (remove digits and meaningless words, convert text into lower case, tokenization)
- POS tagging words (lemmatization may be improved based on POS tag)
- Lemmatize each word (only process noun, verb, adj, adv words)
- Find hyponyms and hypernyms for lemmatized words

This script is tested on Python 3.7.4 and NLTK 3.4.5. The script can be executed either by `python3 test.py` or `bash execute.sh`.

- Author: Wenjia Zhai
- Date: June 04 2021
