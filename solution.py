import re
from pprint import pprint

import nltk
# in case you didn't install the required packages in NLTK:
# nltk.download() # download all in once, or download them on by one:
# nltk.download('stopwords')
# nltk.download('punkt')
# nltk.download('averaged_perceptron_tagger')
# nltk.download('wordnet')
from nltk.corpus import wordnet as wn
from nltk.corpus import stopwords
sw = stopwords.words('english')

from nltk import word_tokenize
from nltk.stem import WordNetLemmatizer
lemmatizer = WordNetLemmatizer()

def preprocessing(text):
    '''
    clean up and tokenize text
    '''
    text = re.sub('[^a-zA-Z]', ' ', text) # remove not needed characters
    tokens = word_tokenize(text.lower().strip()) # tokenize
    
    return tokens

def get_wordnet_pos(treebank_tag):
    '''
    convert POS tag (only noun, verb, adj, adv)
    for every token from TreeBank to WordNet
    so that lemmatizer can understand
    '''
    if treebank_tag.startswith('J'):
        return wn.ADJ
    elif treebank_tag.startswith('V'):
        return wn.VERB
    elif treebank_tag.startswith('N'):
        return wn.NOUN
    elif treebank_tag.startswith('R'):
        return wn.ADV
    return None

def lemmatize(tokens):
    '''
    lemmatize every token based on its POS tag
    '''
    tagged = nltk.pos_tag(tokens)
    lemma_tokens = []

    for word, tag in tagged:
        wntag = get_wordnet_pos(tag)

        if word in sw: # skip stopwords
            continue

        if wntag: # only process noun, verb, adj, adv words
            lemma_tokens.append((lemmatizer.lemmatize(word, pos=wntag), wntag))

    return set(lemma_tokens) # remove duplications

def find_hyponyms_hypernyms(lemma_tokens):
    '''
    search hyponyms and hypernyms for each token.
    1. find synonyms of each word
    2. find hyponyms and hypernyms for each synonym
    3. collect all hyponyms and hypernyms for the same word
    4. remove duplications, store them in a dictionary
    '''
    result = dict()
    for token, tag in lemma_tokens:
        hypo_collect, hyper_collect = [], []
        synsets = wn.synsets(token, tag) # step 1

        for synset in synsets:
            hypos = synset.hyponyms() # step 2
            hyers = synset.hypernyms() # step 2

            for hypo in hypos: # step 3
                hypo_collect += [str(lemma.name()) for lemma in hypo.lemmas()]
            for hyper in hyers: # step 3
                hyper_collect += [str(lemma.name()) for lemma in hy[er.lemmas()]
        
        if hypo_collect and not hyper_collect: # step 4
            result[token] = {'hyponyms':list(set(hypo_collect))}
        elif not hypo_collect and hyper_collect:
            result[token] = {'hypernyms':list(set(hyper_collect))}
        elif hypo_collect and hyper_collect:
            result[token] = {'hyponyms':list(set(hypo_collect)), 'hypernyms':list(set(hyper_collect))}

    return result

if __name__ == '__main__':
    text = '''\tThe differential diagnosis of multiple myeloma usually involves the
    spectrum of plasma cell proliferative disorders shown in Table
    3.1,2,6,13,14,20 A full evaluation will help classify where a patient
    falls in this spectrum.\n The differential diagnosis of bone lesions
    includes primary or metastatic cancer, benign bone lesions,
    osteoporotic compression fracture, and other bone conditions.21,22 The
    full differential diagnosis for patients presenting with fatigue,
    unexplained weight loss, or hypercalcemia is broad and beyond the
    scope of this article.23-25'''

    tokens = preprocessing(text)
    lemma_tokens = lemmatize(tokens)
    result = find_hyponyms_hypernyms(lemma_tokens)

    pprint(result)
